#include "stdafx.h"
#include "CppUnitTest.h"
#include <AStringFormatter.h>
#include <IStringFormatter.h>
#include <string>
#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TDDD04_lab1Btests
{		
	TEST_CLASS(StringTest)
	{
		IStringFormatter *_iSF;
	public:
		TEST_METHOD_INITIALIZE(setUp)
		{
			_iSF = new AStringFormatter();
		}
		TEST_METHOD_CLEANUP(tearDown)
		{
			delete _iSF;
		}

		TEST_METHOD(TestLeftString)
		{
			Assert::IsTrue(_iSF->leftString("testing",10,'+') == "testing+++", L"leftString is wrong");
			Assert::IsTrue(_iSF->leftString("testing", 7, '+') == "testing", L"leftString is wrong");
			Assert::IsTrue(_iSF->leftString("testing", 1, '+') == "t", L"leftString is wrong");
			Assert::IsTrue(_iSF->leftString("testing", 0, '+') == "", L"leftString is wrong");

			// invalid string
			Assert::ExpectException<InvalidStringException>([&]{ _iSF->leftString("", 1, '+'); });
		}

		TEST_METHOD(TestCenterString)
		{
			// ODD
			Assert::IsTrue(_iSF->centerString("odd", 2, '+') == "dd", L"centerString is wrong");
			Assert::IsTrue(_iSF->centerString("odd", 3, '+') == "odd", L"centerString is wrong");
			Assert::IsTrue(_iSF->centerString("odd", 4, '+') == "odd+", L"centerString is wrong");
			Assert::IsTrue(_iSF->centerString("odd", 6, '+') == "+odd++", L"centerString is wrong");
			Assert::IsTrue(_iSF->centerString("odd", 0, '+') == "", L"centerString is wrong");

			// EVEN
			Assert::IsTrue(_iSF->centerString("even", 1, '+') == "e", L"centerString is wrong");
			Assert::IsTrue(_iSF->centerString("even", 2, '+') == "ve", L"centerString is wrong"); 
			Assert::IsTrue(_iSF->centerString("even", 4, '+') == "even", L"centerString is wrong");
			Assert::IsTrue(_iSF->centerString("even", 5, '+') == "even+", L"centerString is wrong");
			Assert::IsTrue(_iSF->centerString("even", 7, '+') == "+even++", L"centerString is wrong");
			Assert::IsTrue(_iSF->centerString("even", 0, '+') == "", L"centerString is wrong");

			// invalid string
			Assert::ExpectException<InvalidStringException>([&]{ _iSF->centerString("", 1, '+'); });
			
		}

		TEST_METHOD(TestRightString)
		{
			Assert::IsTrue(_iSF->rightString("Software", 4, '-') == "ware", L"rightString is wrong");
			Assert::IsTrue(_iSF->rightString("Software", 8, '-') == "Software", L"rightString is wrong");
			Assert::IsTrue(_iSF->rightString("Software", 9, '-') == "-Software", L"rightString is wrong");
			Assert::IsTrue(_iSF->rightString("Software", 0, '-') == "", L"rightString is wrong");

			// invalid string
			Assert::ExpectException<InvalidStringException>([&]{ _iSF->rightString("", 1, '+'); });
		}

	};
}