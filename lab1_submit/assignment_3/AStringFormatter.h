#pragma once
#include <cstdlib>
#include <string>
#include "IStringFormatter.h"
#include "InvalidStringException.h"

class AStringFormatter: public IStringFormatter
{
public:
	AStringFormatter();
	~AStringFormatter();
	std::string centerString(const std::string& s, const size_t textWidth, const char paddingChar);
	std::string leftString(const std::string& s, const size_t textWidth, const char paddingChar);
	std::string rightString(const std::string& s, const size_t textWidth, const char paddingChar);
};

