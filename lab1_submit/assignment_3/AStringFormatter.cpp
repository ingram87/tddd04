#include "AStringFormatter.h"
#include <cstdlib>
#include <string>
using namespace std;

AStringFormatter::AStringFormatter(){}
AStringFormatter::~AStringFormatter(){}

std::string AStringFormatter::centerString(const std::string& s, const size_t textWidth, const char paddingChar)
{	
	std::string res = "";
	int strlen;
	bool flop = true;
	int mid;

	try{
		res = s;
		strlen = s.length();

		if (strlen <= 0) {
			throw InvalidStringException();
		}


		if (textWidth == 0) {
			return "";
		}

		// even, odd check
		if (strlen % 2){
			mid = (strlen + 1) / 2;
		}
		else {
			mid = (strlen) / 2;
		}

		if (textWidth < strlen){
			res = res.substr(mid - floor(textWidth / 2), textWidth);
		}
		else if (textWidth == strlen) {
			res = s;
		}
		else {
			for (int i = 0; i < (textWidth - strlen); i++){
				if (flop) {
					res = res + paddingChar;
					flop = false;
				}
				else {
					res = paddingChar + res;
					flop = true;
				}
			}
		}



	}
	catch (...){
		// might be invalid input etc
		res = "Something is wrong.";
		throw InvalidStringException();
	}

	return res;
}

std::string AStringFormatter::leftString(const std::string& s, const size_t textWidth, const char paddingChar)
{
	std::string res;
	int strlen;
	int padding_length;

	try {
		res = s;
		strlen = res.length();
		
		if (strlen <= 0) {
			throw InvalidStringException();
		}

		if (textWidth == 0) {
			return "";
		}

		padding_length = textWidth - strlen;

		if (padding_length >= 0){
			for (int i = 0; i < padding_length; i++){
				res = res + paddingChar;
			}
		}
		else {
			for (int i = padding_length; i < 0; i++){
				res.pop_back();
			}
		}

	}
	catch (...){
		// might be invalid input etc
		res = "Something is wrong.";
		throw InvalidStringException();
	}

	return res;
}

std::string AStringFormatter::rightString(const std::string& s, const size_t textWidth, const char paddingChar)
{
	std::string res;
	int strlen;
	int padding_length;

	try {
		res = s;
		strlen = res.length();

		if (strlen <= 0) {
			throw InvalidStringException();
		}

		if (textWidth == 0) {
			return "";
		}

		padding_length = textWidth - strlen;

		// adding paddingChar to the left of the string
		if (padding_length >= 0){
			for (int i = 0; i < padding_length; i++){
				res = paddingChar + res;
			}
		}
		else {
			res = res.substr(abs(padding_length), strlen);
		}


	}
	catch (...){
		// might be invalid input etc
		res = "Something is wrong.";
		throw InvalidStringException();
	}

	return res;
}
