#include "stdafx.h"
#include "CppUnitTest.h"
#include <triangle.h>
#include "ToStringMap.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;



namespace TraingleTest
{		
	TEST_CLASS(TraingleTest)
	{
		Triangle *_triangle;
	public:
		TEST_METHOD_INITIALIZE(setUp)
		{
			_triangle = new Triangle();
		}
		TEST_METHOD_CLEANUP(tearDown)
		{
			delete _triangle;
		}

		/*
		insert your test implementation here
		*/
		TEST_METHOD(testScalene)
		{
			// valid scalenes
			int sides[3] = { 3, 4, 5 };
			Assert::AreEqual(Triangle::SCALENE, _triangle->getType(sides, 3), L"Trangle is not scalene");
			int sides2[3] = { 3, 5, 4 };
			Assert::AreEqual(Triangle::SCALENE, _triangle->getType(sides2, 3), L"Trangle is not scalene");
			int sides3[3] = { 5, 3, 4 };
			Assert::AreEqual(Triangle::SCALENE, _triangle->getType(sides3, 3), L"Trangle is not scalene");

			// invalid scalenes
			int sides4[3] = { 1, 2, 3 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides4, 3); });
			int sides5[3] = { 1, 3, 2 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides5, 3); });
			int sides6[3] = { 3, 1, 2 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides6, 3); });

			int sides7[3] = { 1, 2, 4 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides7, 3); });
			int sides8[3] = { 1, 4, 2 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides8, 3); });
			int sides9[3] = { 4, 1, 2 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides9, 3); });
		}

		TEST_METHOD(testIsosceles)
		{
			// Valid isosceles
			int sides[3] = { 2, 2, 3 };
			Assert::AreEqual(Triangle::ISOSCELES, _triangle->getType(sides, 3), L"Trangle is not isosceles");
			int sides2[3] = { 2, 3, 2 };
			Assert::AreEqual(Triangle::ISOSCELES, _triangle->getType(sides2, 3), L"Trangle is not isosceles");
			int sides3[3] = { 3, 3, 2 };
			Assert::AreEqual(Triangle::ISOSCELES, _triangle->getType(sides3, 3), L"Trangle is not isosceles");
	

			// Invalid isosceles
			int sides4[3] = { 1, 1, 2 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides4, 3); });
			int sides5[3] = { 1, 2, 1 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides5, 3); });
			int sides6[3] = { 2, 1, 1 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides6, 3); });

			int sides7[3] = { 1, 1, 3 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides4, 3); });
			int sides8[3] = { 1, 3, 1 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides4, 3); });
			int sides9[3] = { 3, 1, 1 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides4, 3); });
		}

		TEST_METHOD(testEquilateral)
		{
			int sides[3] = { 3, 3, 3 };
			Assert::AreEqual(Triangle::EQUILATERAL, _triangle->getType(sides, 3), L"Trangle is not equlaterial");
		}

		TEST_METHOD(testZero)
		{
			int sides[3] = { 0, 1, 2 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides, 3); });
			int sides2[3] = { 1, 0, 2 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides2, 3); });
			int sides3[3] = { 1, 2, 0 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides3, 3); });
			int sides4[4] = { 0, 0, 0 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides4, 3); });
		}

		TEST_METHOD(testNegative)
		{
			int sides[3] = { -1, 2, 1 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides, 3); });
			int sides2[3] = { 2, -1, 1 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides2, 3); });
			int sides3[3] = { 1, 2, -1 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides3, 3); });
		}

		TEST_METHOD(testInputLengthError)
		{
			int sides1[2] = { 1, 2 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides1, 2); });
			int sides2[4] = { 1, 2, 3, 4 };
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides2, 4); });
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides2, 0); });
			Assert::ExpectException<InvalidTriangleException>([&]{ _triangle->getType(sides2, -1); });
		}

		TEST_METHOD(testTypeConversion)
		{
			Assert::IsTrue("Isosceles" == _triangle->TypeToString(_triangle->ISOSCELES), L"Conversion of isoscelesss failed");
			Assert::IsTrue("Equilateral" == _triangle->TypeToString(_triangle->EQUILATERAL), L"Conversion of equilateral failed");
			Assert::IsTrue("Scalene" == _triangle->TypeToString(_triangle->SCALENE), L"Conversion of scalene failed");
			Assert::IsTrue("unknown triangle type" == _triangle->TypeToString(_triangle->INVALID), L"Conversion of scalene failed");
		}

	};
}