@echo off
echo 3 4 5 | triangle.exe
echo 3 5 4 | triangle.exe
echo 5 4 3 | triangle.exe

echo 3 3 4 | triangle.exe

echo 3 3 3 | triangle.exe

echo 4 3 3 | triangle.exe
echo 3 4 3 | triangle.exe
echo 3 3 4 | triangle.exe

echo 0 1 2 | triangle.exe

echo -1 2 1 | triangle.exe

echo 2 -1 1 | triangle.exe
echo 2 1 -1 | triangle.exe

echo 3 3 6 | triangle.exe

echo 3 4 7 | triangle.exe

echo 1 2 3 | triangle.exe
echo 1 3 2 | triangle.exe
echo 3 1 2 | triangle.exe

echo 1 1 2 | triangle.exe
echo 1 2 1 | triangle.exe
echo 2 1 1 | triangle.exe

echo 1 1 3 | triangle.exe

echo 1 2 4 | triangle.exe

echo 3 1 1 | triangle.exe
echo 1 3 1 | triangle.exe

echo 1 4 2 | triangle.exe
echo 4 1 2 | triangle.exe

echo 0 0 0 | triangle.exe

echo a b c | triangle.exe

echo | triangle.exe

echo 1 2 | triangle.exe

echo 1 2 3 4 | triangle.exe